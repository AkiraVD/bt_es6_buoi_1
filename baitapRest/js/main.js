function tinhDTB(...nums) {
  let sum = 0;
  nums.forEach((item) => {
    sum += item;
  });

  return sum / nums.length;
}

function DTBKhoi1() {
  let inpToan = document.getElementById("inpToan").value * 1;
  let inpLy = document.getElementById("inpLy").value * 1;
  let inpHoa = document.getElementById("inpHoa").value * 1;

  let dtb = tinhDTB(inpToan, inpLy, inpHoa);
  document.getElementById("tbKhoi1").innerText = dtb.toFixed(2);
}

function DTBKhoi2() {
  let inpVan = document.getElementById("inpVan").value * 1;
  let inpSu = document.getElementById("inpSu").value * 1;
  let inpDia = document.getElementById("inpDia").value * 1;
  let inpEnglish = document.getElementById("inpEnglish").value * 1;

  let dtb = tinhDTB(inpVan, inpSu, inpDia, inpEnglish);
  document.getElementById("tbKhoi2").innerText = dtb.toFixed(2);
}
