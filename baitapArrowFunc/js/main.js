const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let contentColorList = "";
colorList.forEach((item) => {
  contentColorList += `
<button class="color-button ${item}" onclick=addColor("${item}")></button>
`;
});

document.getElementById("colorContainer").innerHTML = contentColorList;

let currentColor = null;
function addColor(color) {
  document.getElementById("house").classList.remove(currentColor);
  document.getElementById("house").classList.add(color);
  currentColor = color;
}
