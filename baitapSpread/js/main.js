let str = document.querySelector("h2.heading").innerHTML;

let strArr = [...str];
console.log("arr: ", strArr);

let contentHTML = "";

strArr.forEach((item) => {
  contentHTML += `
    <span>${item}</span>
    `;
});

document.querySelector("h2.heading").innerHTML = contentHTML;
